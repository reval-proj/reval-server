'use strict';
const http = require('http');

http.createServer(function(request, response){
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept');
    response.writeHead(200, {'Content-Type': 'text/html'});


    request.params = params(request);

    var tempData = {
        1: "Санкт-Петербург",
        2: "Саратов",
        3: "Сочи",
        4: "Судак",
        5: "Смоленск",
        6: "Александровск-Сахалинский"
    }
    // for(var k in tempData) {
    //     tempData[k] = request.params.value;
    // }

    response.write(JSON.stringify({code:200, tempStr: tempData}));
    response.end();
}).listen(8080);
console.log('Server running on 8080');


var params=function(req){
    let q=req.url.split('?'),result={};
    if(q.length>=2){
        q[1].split('&').forEach((item)=>{
             try {
               result[item.split('=')[0]]=item.split('=')[1];
             } catch (e) {
               result[item.split('=')[0]]='';
             }
        })
    }
    return result;
  }